/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expression.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbaelor- <bbaelor-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/07 14:08:43 by bwerewol          #+#    #+#             */
/*   Updated: 2019/03/20 11:47:48 by bbaelor-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EXPRESSION_H
# define EXPRESSION_H

# include <stdint.h>
# include <stdio.h>
# include "shell.h"

/*
** typedef struct			s_astree
** {
** 	int				type;
** 	char			*content;
** 	struct s_astree	*left;
** 	struct s_astree	*right;
** }						t_astree;
*/

extern unsigned int		g_excurtok;
extern void				*g_extokens;
extern int				g_exprerr;

t_astree				*expr(void);
intmax_t				calc(t_astree *root);
char					*expression(char *arg);

#endif
